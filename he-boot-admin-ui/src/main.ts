import { createApp } from 'vue';
import pinia from '/@/stores/index';
import App from '/@/App.vue';
import router from '/@/router';
import { directive } from '/@/directive/index';
import { i18n } from '/@/i18n/index';
import other from '/@/utils/other';

import ElementPlus from 'element-plus';
import '/@/theme/index.scss';
import VueGridLayout from 'vue-grid-layout';
import { useDict } from '/@/utils/dict'
import {
    parseTime,
    resetForm,
    addDateRange,
    handleTree,
    selectDictLabel,
    selectDictLabels
} from '/@/utils/heboot'
import plugins from './plugins' // plugins

// 分页组件
import Pagination from '/@/components/Pagination/index.vue'
// 自定义表格工具组件
import RightToolbar from '/@/components/RightToolbar/index.vue'
// 自定义树选择组件
import TreeSelect from '/@/components/TreeSelect/index.vue'
// 字典标签组件
import DictTag from '/@/components/DictTag/index.vue'

const app = createApp(App);

other.elSvg(app);
// 全局方法挂载
app.config.globalProperties.useDict = useDict
app.config.globalProperties.parseTime = parseTime
app.config.globalProperties.resetForm = resetForm
app.config.globalProperties.handleTree = handleTree
app.config.globalProperties.addDateRange = addDateRange
app.config.globalProperties.selectDictLabel = selectDictLabel
app.config.globalProperties.selectDictLabels = selectDictLabels
// 全局组件挂载
app.component('DictTag', DictTag)
app.component('Pagination', Pagination)
app.component('TreeSelect', TreeSelect)
app.component('RightToolbar', RightToolbar)
directive(app)
app.use(plugins).use(pinia).use(router).use(ElementPlus,{ size:'small'}).use(i18n).use(VueGridLayout).mount('#app');

