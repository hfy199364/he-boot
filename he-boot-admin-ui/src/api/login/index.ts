import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 登录api接口集合
 * @method signIn 用户登录
 * @method signOut 用户退出登录
 */
export function useLoginApi() {
    return {
        signIn: (data: object) => {
            // 提供默认值，使用对象扩展语法
            const defaultData = {
                tenantId: '1',
                clientId: 'e5cd7e4891bf95d1d19206ce24a7b32e',
                grantType: 'password',
            };

            // 合并默认值和传入的值
            const mergedData = {...defaultData, ...data};

            return request({
                url: '/login',
                method: 'post',
                data: mergedData,
            });
        },
        signOut: (data: object) => {
            return request({
                url: '/logout',
                method: 'post',
                data,
            });
        },
        // 获取用户详细信息
        getInfo: () => {
            return request({
                url: '/system/user/getInfo',
                method: 'get'
            });
        },
        getCodeImg: (params?: object) => {
            return request({
                url: '/captchaImage',
                method: 'get',
                params,
            });
        },
    };
}
