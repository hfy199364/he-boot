import {useLoginApi} from '/@/api/login';

import {getToken, setToken, removeToken} from '/@/utils/auth'
import defAva from '/@/assets/logo-mini.svg'
import {defineStore} from 'pinia'
import {IUserInfo} from './type/index'

const useUserStore = defineStore('user', {
    state: () => ({
        token: getToken(),
        name: '',
        avatar: '',
        roles: [] as string[],
        permissions: []
    }),
    actions: {
        // 登录
        login(userInfo: IUserInfo) {
            const username = userInfo.username.trim()
            const password = userInfo.password
            const code = userInfo.code
            const uuid = userInfo.uuid
            return new Promise<void>((resolve, reject) => {
                let data = {username: username, password: password, code: code, uuid: uuid}
                useLoginApi().signIn(data)
                    .then((res) => {
                        let data = res.data
                        setToken(data.access_token)
                        this.token = data.access_token
                        resolve()
                    })
                    .catch((error) => {
                        reject(error)
                    })
            })
        },
        // 获取用户信息
        getInfo() {
            return new Promise((resolve, reject) => {
                useLoginApi().getInfo()
                    .then((res: any) => {
                        const user = res.user
                        const avatar =
                            user.avatar == '' || user.avatar == null ? defAva : user.avatar

                        if (res.roles && res.roles.length > 0) {
                            // 验证返回的roles是否是一个非空数组
                            this.roles = res.roles
                            this.permissions = res.permissions
                        } else {
                            this.roles = ['ROLE_DEFAULT']
                        }
                        this.name = user.userName
                        this.avatar = avatar
                        resolve(res)
                    })
                    .catch((error) => {
                        reject(error)
                    })
            })
        },
        // 退出系统
        logOut() {
            return new Promise<void>((resolve, reject) => {
                useLoginApi().signOut({})
                    .then(() => {
                        this.token = ''
                        this.roles = []
                        this.permissions = []
                        removeToken()
                        resolve()
                    })
                    .catch((error) => {
                        reject(error)
                    })
            })
        }
    }
})

export default useUserStore
