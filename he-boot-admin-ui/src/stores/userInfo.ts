import {defineStore} from 'pinia';
import {Session} from '/@/utils/storage';
import {useLoginApi} from "/@/api/login";
import defAva from "/@/assets/logo-mini.svg";

/**
 * 用户信息
 * @methods setUserInfos 设置用户信息
 */
export const useUserInfo = defineStore('userInfo', {
    state: (): UserInfosState => ({
        userInfos: {
            userName: '',
            photo: '',
            time: 0,
            roles: [],
            authBtnList: [],
        },
    }),
    actions: {
        async setUserInfos() {
            // 存储用户信息到浏览器缓存
            if (Session.get('userInfo')) {
                this.userInfos = Session.get('userInfo');
            } else {
                const userInfos = <UserInfos>await this.getInfo();
                this.userInfos = userInfos;
            }
        },
        // 获取用户信息
        async getInfo() {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    useLoginApi().getInfo()
                        .then((res: any) => {
                            const user = res.data.user
                            const avatar = user.url == '' || user.url == null ? defAva : user.url
                            if (res.data.roles && res.data.roles.length > 0) {
                                // 验证返回的roles是否是一个非空数组
                                this.userInfos.roles = res.data.roles
                                this.userInfos.authBtnList = res.data.permissions
                            } else {
                                this.userInfos.roles = ['ROLE_DEFAULT']
                            }
                            this.userInfos.userName = user.userName
                            this.userInfos.photo = avatar
                            Session.set('userInfo', this.userInfos);
                            resolve(this.userInfos);
                        })
                        .catch((error) => {
                            reject(error)
                        })
                }, 0);
            })
        },
    },
});
