-- sys_menu菜单表结构修改、去掉主键自增
ALTER TABLE `sys_menu`
	CHANGE COLUMN `menu_id` `menu_id` BIGINT(19) NOT NULL COMMENT '菜单ID' FIRST,
	CHANGE COLUMN `order_num` `order_num` INT(4) NULL DEFAULT '0' COMMENT '显示顺序' AFTER `parent_id`,
	CHANGE COLUMN `query` `query_param` VARCHAR(255) NULL DEFAULT NULL COMMENT '路由参数' COLLATE 'utf8mb4_bin' AFTER `component`,
	CHANGE COLUMN `is_frame` `is_frame` INT(1) NULL DEFAULT '1' COMMENT '是否为外链（0是 1否）' AFTER `query_param`,
	CHANGE COLUMN `is_cache` `is_cache` INT(1) NULL DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）' AFTER `is_frame`;


