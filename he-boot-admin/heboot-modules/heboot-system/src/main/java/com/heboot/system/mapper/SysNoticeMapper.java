package com.heboot.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysNotice;
import org.apache.ibatis.annotations.Mapper;

/**
 * 通知公告表 数据层
 *
 * @author 数据小王子
 */
@Mapper
public interface SysNoticeMapper  extends BaseMapper<SysNotice>
{

}
