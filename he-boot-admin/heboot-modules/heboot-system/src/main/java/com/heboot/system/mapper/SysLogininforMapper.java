package com.heboot.system.mapper;

import java.util.List;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysLogininfor;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysLogininforMapper extends BaseMapper<SysLogininfor>
{

}
