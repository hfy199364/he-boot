package com.heboot.system.mapper;

import java.util.List;

import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.heboot.system.domain.SysUserRole;

/**
 * 用户与角色关联表 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole>
{


}
