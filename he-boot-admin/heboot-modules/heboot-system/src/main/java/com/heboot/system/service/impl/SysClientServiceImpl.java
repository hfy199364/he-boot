package com.heboot.system.service.impl;


import org.springframework.stereotype.Service;
import com.heboot.system.service.ISysClientService;
import com.heboot.system.domain.SysClient;
import com.heboot.system.mapper.SysClientMapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;

/**
 * 系统授权表 服务层实现。
 *
 * @author mybatis-flex-helper automatic generation
 * @since 1.0
 */
@Service
public class SysClientServiceImpl extends ServiceImpl<SysClientMapper, SysClient> implements ISysClientService {

}
