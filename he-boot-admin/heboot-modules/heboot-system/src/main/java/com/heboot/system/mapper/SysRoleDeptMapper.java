package com.heboot.system.mapper;

import java.util.List;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysRoleDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色与部门关联表 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept>
{

}
