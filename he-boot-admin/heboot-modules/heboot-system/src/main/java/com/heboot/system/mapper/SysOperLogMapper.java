package com.heboot.system.mapper;


import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysOperLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 操作日志 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysOperLogMapper extends BaseMapper<SysOperLog>
{

}
