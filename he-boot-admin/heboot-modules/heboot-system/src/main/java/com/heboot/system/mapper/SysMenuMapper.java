package com.heboot.system.mapper;


import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 菜单表 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu>
{

}
