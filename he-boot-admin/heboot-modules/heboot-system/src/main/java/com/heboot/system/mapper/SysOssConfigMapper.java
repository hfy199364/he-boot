package com.heboot.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysOssConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * 对象存储配置Mapper接口
 *
 * @author hfy
 * @author 孤舟烟雨
 * @date 2021-08-13
 */
@Mapper
public interface SysOssConfigMapper extends BaseMapper<SysOssConfig> {

}
