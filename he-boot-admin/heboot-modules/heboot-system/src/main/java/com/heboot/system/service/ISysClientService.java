package com.heboot.system.service;


import com.heboot.system.domain.SysClient;
import com.mybatisflex.core.service.IService;

/**
 * 系统授权表 服务层。
 *
 * @author mybatis-flex-helper automatic generation
 * @since 1.0
 */
public interface ISysClientService extends IService<SysClient> {

}
