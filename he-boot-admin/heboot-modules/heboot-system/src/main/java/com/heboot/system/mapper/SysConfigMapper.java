package com.heboot.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * 参数配置Mapper
 *
 * @author 数据小王子
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig>
{

}
