package com.heboot.system.mapper;

import java.util.List;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysPost;
import org.apache.ibatis.annotations.Mapper;

/**
 * 岗位信息 数据层
 *
 * @author 数据小王子
 */
@Mapper
public interface SysPostMapper extends BaseMapper<SysPost>
{

}
