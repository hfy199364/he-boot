package com.heboot.system.mapper;

import java.util.List;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysUser;
import com.heboot.system.domain.vo.SysUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 用户表 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser>
{

}
