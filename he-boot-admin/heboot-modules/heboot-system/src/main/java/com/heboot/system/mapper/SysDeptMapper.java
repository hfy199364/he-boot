package com.heboot.system.mapper;

import java.util.List;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysDept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 部门管理 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept>
{

}
