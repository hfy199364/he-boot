package com.heboot.system.controller.system;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.heboot.common.core.core.domain.R;
import com.heboot.common.core.validate.AddGroup;
import com.heboot.common.core.validate.EditGroup;
import com.heboot.common.core.validate.QueryGroup;
import com.heboot.common.web.annotation.RepeatSubmit;
import com.heboot.common.web.core.BaseController;
import com.heboot.common.log.annotation.Log;
import com.heboot.common.log.enums.BusinessType;
import com.heboot.common.orm.core.page.PageQuery;
import com.heboot.common.orm.core.page.TableDataInfo;
import com.heboot.system.domain.bo.SysOssConfigBo;
import com.heboot.system.domain.vo.SysOssConfigVo;
import com.heboot.system.service.ISysOssConfigService;
import jakarta.annotation.Resource;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 对象存储配置
 *
 * @author hfy
 * @author 孤舟烟雨
 * @author 数据小王子
 * @date 2023-11-30
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/resource/oss/config")
public class SysOssConfigController extends BaseController {

    @Resource
    private final ISysOssConfigService ossConfigService;

    /**
     * 查询对象存储配置列表
     */
    @SaCheckPermission("system:oss:list")
    @GetMapping("/list")
    public TableDataInfo<SysOssConfigVo> list(SysOssConfigBo bo) {
        return ossConfigService.queryPageList(bo);
    }

    /**
     * 获取对象存储配置详细信息
     *
     * @param ossConfigId OSS配置ID
     */
    @SaCheckPermission("system:oss:query")
    @GetMapping("/{ossConfigId}")
    public R<SysOssConfigVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long ossConfigId) {
        return R.ok(ossConfigService.queryById(ossConfigId));
    }

    /**
     * 新增对象存储配置
     */
    @SaCheckPermission("system:oss:add")
    @Log(title = "对象存储配置", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated @RequestBody SysOssConfigBo bo) {
        boolean inserted = ossConfigService.insertByBo(bo);
        if (!inserted) {
            return R.fail("新增对象存储配置记录失败！");
        }
        return R.ok();
    }

    /**
     * 修改对象存储配置
     */
    @SaCheckPermission("system:oss:edit")
    @Log(title = "对象存储配置", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated @RequestBody SysOssConfigBo bo) {
        Boolean updated = ossConfigService.updateByBo(bo);
        if (!updated) {
            R.fail("修改对象存储配置记录失败!");
        }
        return R.ok();
    }

    /**
     * 删除对象存储配置
     *
     * @param ossConfigIds OSS配置ID串
     */
    @SaCheckPermission("system:oss:remove")
    @Log(title = "对象存储配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ossConfigIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ossConfigIds) {
        boolean deleted = ossConfigService.deleteWithValidByIds(List.of(ossConfigIds), true);
        if (!deleted) {
            R.fail("删除对象存储配置记录失败!");
        }
        return R.ok();
    }

    /**
     * 状态修改
     */
    @SaCheckPermission("system:oss:edit")
    @Log(title = "对象存储状态修改", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public R<Void> changeStatus(@RequestBody SysOssConfigBo bo) {
        boolean updated = ossConfigService.updateOssConfigStatus(bo);
        if (!updated) {
            R.fail("状态修改失败!");
        }
        return R.ok();
    }
}
