package com.heboot.system.mapper;

import java.util.List;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysUserPost;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户与岗位关联表 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysUserPostMapper extends BaseMapper<SysUserPost>
{

}
