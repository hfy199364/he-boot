package com.heboot.system.service;

import com.heboot.common.orm.core.page.TableDataInfo;
import com.heboot.common.orm.core.service.IBaseService;
import com.heboot.system.domain.SysOss;
import com.heboot.system.domain.bo.SysOssBo;
import com.heboot.system.domain.vo.SysOssVo;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * 文件上传 服务层
 *
 * @author hfy
 */
public interface ISysOssService extends IBaseService<SysOss> {

    TableDataInfo<SysOssVo> queryPageList(SysOssBo sysOss);

    List<SysOssVo> listSysOssByIds(Collection<Long> ossIds);

    SysOssVo getById(Long ossId);

    SysOssVo upload(MultipartFile file);

    SysOssVo upload(File file);

    void download(Long ossId, HttpServletResponse response) throws IOException;

    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

}
