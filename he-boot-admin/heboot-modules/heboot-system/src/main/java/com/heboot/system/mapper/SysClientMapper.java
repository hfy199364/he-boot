package com.heboot.system.mapper;

import com.heboot.system.domain.SysClient;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统授权表 映射层。
 *
 * @author mybatis-flex-helper automatic generation
 * @since 1.0
 */
@Mapper
public interface SysClientMapper extends BaseMapper<SysClient> {


}
