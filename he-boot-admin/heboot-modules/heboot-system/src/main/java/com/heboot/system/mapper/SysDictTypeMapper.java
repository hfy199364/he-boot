package com.heboot.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysDictType;
import org.apache.ibatis.annotations.Mapper;


/**
 * 字典表 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysDictTypeMapper  extends BaseMapper<SysDictType>
{

}
