package com.heboot.system.mapper;

import java.util.List;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色与菜单关联表 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu>
{


}
