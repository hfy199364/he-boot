package com.heboot.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.heboot.system.domain.SysOss;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传 数据层
 *
 * @author hfy
 */
@Mapper
public interface SysOssMapper extends BaseMapper<SysOss> {
}
