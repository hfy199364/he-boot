package com.heboot.web.service.impl;

import cn.dev33.satoken.stp.SaLoginModel;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ObjectUtil;
import com.heboot.common.core.core.domain.AjaxResult;
import com.heboot.system.domain.vo.SysUserVo;
import com.heboot.system.service.ISysUserService;
import com.heboot.web.domain.vo.LoginVo;
import jakarta.annotation.Resource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.heboot.common.core.constant.Constants;
import com.heboot.common.core.constant.GlobalConstants;
import com.heboot.common.core.core.domain.model.LoginBody;
import com.heboot.common.core.core.domain.model.LoginUser;
import com.heboot.common.core.enums.LoginType;
import com.heboot.common.core.enums.UserStatus;
import com.heboot.common.core.exception.user.CaptchaExpireException;
import com.heboot.common.core.exception.user.UserException;
import com.heboot.common.core.utils.MessageUtils;
import com.heboot.common.core.utils.StringUtils;
import com.heboot.common.core.utils.ValidatorUtils;
import com.heboot.common.core.validate.auth.EmailGroup;
import com.heboot.common.redis.utils.RedisUtils;
import com.heboot.common.security.utils.LoginHelper;
import com.heboot.system.domain.SysClient;
import com.heboot.system.domain.SysUser;
import com.heboot.system.mapper.SysUserMapper;
import com.heboot.web.service.IAuthStrategy;
import com.heboot.web.service.SysLoginService;
import org.springframework.stereotype.Service;

/**
 * 邮件认证策略
 *
 * @author Michelle.Chung
 */
@Slf4j
@Service("email" + IAuthStrategy.BASE_NAME)
@RequiredArgsConstructor
public class EmailAuthStrategy implements IAuthStrategy {

    @Resource
    private final SysLoginService loginService;

    @Resource
    private ISysUserService userService;
//    private final SysUserMapper userMapper;

    @Override
    public void validate(LoginBody loginBody) {
        ValidatorUtils.validate(loginBody, EmailGroup.class);
    }

    @Override
    public LoginVo login(String clientId, LoginBody loginBody, SysClient client) {
        Long tenantId = loginBody.getTenantId();
        String email = loginBody.getEmail();
        String emailCode = loginBody.getEmailCode();

        // 通过邮箱查找用户
        SysUserVo user = loadUserByEmail(tenantId, email);

        loginService.checkLogin(LoginType.EMAIL, tenantId, user.getUserName(), () -> !validateEmailCode(tenantId, email, emailCode));
        // 此处可根据登录用户的数据不同 自行创建 loginUser 属性不够用继承扩展就行了
        LoginUser loginUser = loginService.buildLoginUser(user);
        SaLoginModel model = new SaLoginModel();
        model.setDevice(client.getDeviceType());
        // 自定义分配 不同用户体系 不同 token 授权时间 不设置默认走全局 yml 配置
        // 例如: 后台用户30分钟过期 app用户1天过期
        model.setTimeout(client.getTimeout());
        model.setActiveTimeout(client.getActiveTimeout());
        model.setExtra(LoginHelper.CLIENT_KEY, clientId);
        // 生成token
        LoginHelper.login(loginUser, model);

        loginService.recordLogininfor(loginUser.getTenantId(), user.getUserName(), Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success"));
        loginService.recordLoginInfo(user.getUserId());

        LoginVo loginVo = new LoginVo();
        loginVo.setAccessToken(StpUtil.getTokenValue());
        loginVo.setExpireIn(StpUtil.getTokenTimeout());
        loginVo.setClientId(clientId);
        return loginVo;
//        return StpUtil.getTokenValue();
    }

    /**
     * 校验邮箱验证码
     */
    private boolean validateEmailCode(Long tenantId, String email, String emailCode) {
        String code = RedisUtils.getCacheObject(GlobalConstants.CAPTCHA_CODE_KEY + email);
        if (StringUtils.isBlank(code)) {
            loginService.recordLogininfor(tenantId, email, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire"));
            throw new CaptchaExpireException();
        }
        return code.equals(emailCode);
    }

    private SysUserVo loadUserByEmail(Long tenantId, String email) {
//        SysUser user = userMapper.selectOne(new LambdaQueryWrapper<SysUser>()
//            .select(SysUser::getEmail, SysUser::getStatus)
//            .eq(TenantHelper.isEnable(), SysUser::getTenantId, tenantId)
//            .eq(SysUser::getEmail, email));
        SysUserVo user =userService.selectUserByEmail(email);
        if (ObjectUtil.isNull(user)) {
            log.info("登录用户：{} 不存在.", email);
            throw new UserException("user.not.exists", email);
        } else if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            log.info("登录用户：{} 已被停用.", email);
            throw new UserException("user.blocked", email);
        }
//        if (TenantHelper.isEnable()) {
//            return userMapper.selectTenantUserByEmail(email, tenantId);
//        }
        return user;
    }

}
