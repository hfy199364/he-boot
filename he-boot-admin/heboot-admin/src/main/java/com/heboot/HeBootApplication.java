package com.heboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动程序
 *
 * @author hfy
 */
@SpringBootApplication
public class HeBootApplication {
    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(HeBootApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  He-Boot启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
            "\n");
    }
}
