package com.heboot.common.log.enums;

/**
 * 操作状态
 *
 * @author hfy
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
