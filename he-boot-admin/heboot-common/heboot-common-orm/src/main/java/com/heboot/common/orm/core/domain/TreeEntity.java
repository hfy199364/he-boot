package com.heboot.common.orm.core.domain;

import com.mybatisflex.annotation.Column;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * Tree基类
 *
 * @author hfy
 */
public class TreeEntity extends BaseEntity
{
    @Serial
    private static final long serialVersionUID = 1L;

    /** 父菜单名称 */
    @Column(ignore = true)
    private String parentName;

    /** 父菜单ID */
    private Long parentId;

    /** 显示顺序 */
    private Integer orderNum;

    /** 祖级列表 */
    @Column(ignore = true)
    private String ancestors;

    /** 子部门 */
    @Column(ignore = true)
    private List<Object> children = new ArrayList<>();

    public String getParentName()
    {
        return parentName;
    }

    public void setParentName(String parentName)
    {
        this.parentName = parentName;
    }

    public Long getParentId()
    {
        return parentId;
    }

    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Integer getOrderNum()
    {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum)
    {
        this.orderNum = orderNum;
    }

    public String getAncestors()
    {
        return ancestors;
    }

    public void setAncestors(String ancestors)
    {
        this.ancestors = ancestors;
    }

    public List<Object> getChildren()
    {
        return children;
    }

    public void setChildren(List<Object> children)
    {
        this.children = children;
    }
}
