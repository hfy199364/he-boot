package com.heboot.common.orm.listener;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpStatus;
import com.mybatisflex.annotation.UpdateListener;
import com.heboot.common.core.exception.ServiceException;
import com.heboot.common.orm.core.domain.BaseEntity;
import com.heboot.common.security.utils.LoginHelper;

import java.util.Date;

/**
 * Entity实体类全局更新数据监听器
 *
 * @author dataprince数据小王子
 */
public class EntityUpdateListener implements UpdateListener {
    @Override
    public void onUpdate(Object entity) {
        try {
            if (ObjectUtil.isNotNull(entity) && (entity instanceof BaseEntity)) {
                BaseEntity baseEntity = (BaseEntity) entity;
                baseEntity.setUpdateBy(LoginHelper.getUserId());
                baseEntity.setUpdateTime(new Date());
            }
        } catch (Exception e) {
            throw new ServiceException("全局更新数据监听器注入异常 => " + e.getMessage(), HttpStatus.HTTP_UNAUTHORIZED);
        }

    }
}
