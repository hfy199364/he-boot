package com.heboot.common.core.core.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户注册对象
 *
 * @author hfy
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RegisterBody extends LoginBody
{
    private String userType;
}
